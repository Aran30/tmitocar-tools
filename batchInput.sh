#!/bin/bash

# Remember to create the auth.json file. Use the file auth-example.json as a template.

printhelp () {
  echo "Usage: ./batchInput.sh -d directory [-o outputFormat] [-a authFile] [-L language]"
  echo "This script sends all files inside the directory to t-mitocar. It supports txt and pdf files."
  echo "Supported languages are en and de"
  echo "Possible output formats are: png, pdf, xls, csv, svg, dot, txt, json"
  echo
  echo "This script is currently only supporting to request T-Mitocar graphs. It can not store models server side or compare different texts"
}

if (($# == 0))
then
    echo "No arguments provieded!"
    printhelp
    exit 1;
fi

while getopts hd:o:a:L: options ; do
 case "${options}" in
 o) format=${OPTARG};;
 d)
    if [ -d $OPTARG ] ; then
      directory=${OPTARG}
    else
      echo "Specified directory does not exist!"
      exit 1 ;
    fi
    ;;
 a) authfile=${OPTARG};;
 L) language=${OPTARG};;
 h) printhelp; exit ;;
 :)
    echo "Error: -${OPTARG} requires an argument."
    exit 1
    ;;
 *) exit;;
 \?) echo "Unknown option: -$OPTARG" >&2; exit 1 ;;
 esac
done

format=${format:-png}
language=${language:-de}

if [ -z $directory ] ; then
  echo "Option -d is required!"
  exit 1 ;
fi

for filename in $directory/*; do
  ending=${filename##*.}
  echo $filename
  if [ $ending == "txt" ] || [ $ending == "pdf" ] ; then
    ./tmitocar.sh -i $filename -o $format -a $authfile -L $language -s &
    sleep 0.1
  else
    echo Error: $filename is not a txt or pdf file. Skipping this file!
  fi
done

wait
