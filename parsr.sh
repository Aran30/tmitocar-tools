#!/bin/bash

printhelp () {
  echo Usage: ./parsr.sh -i inputFile
  echo This script currently supports pdf files
}

if (($# == 0))
then
    echo "No arguments provided!"
    printhelp
    exit 1;
fi

while getopts hi: options ; do
 case "${options}" in
 i)
    if [ -f $OPTARG ] ; then
      inputfile=${OPTARG}
    else
      echo "Specified file does not exist!"
      exit 1 ;
    fi
    ;;
 h) printhelp; exit ;;
 :)
    echo "Error: -${OPTARG} requires an argument."
    exit 1
    ;;
 *) exit;;
 \?) echo "Unknown option: -$OPTARG" >&2; exit 1 ;;
 esac
done

echo ${inputfile}

curl -X POST http://localhost:3001/api/v1/document -H "Content-Type: multipart/form-data" -F "file=@${inputfile};type=application/pdf" -F "config=@parsr-default-config.json;type=application/json"

