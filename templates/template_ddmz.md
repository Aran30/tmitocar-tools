# Manual für das graphenbasierte Feedback in der VL Bildungstechnologien I (TASA / T-TMITOCAR)

## Zwei Texte zum Thema -subject-

Dieses Dokument enthält eine Analyse zu deiner Schreibaufgabe zum Thema -subject-. Die Analyse wurde für dich automatisch und ausschließlich auf der Grundlage der von dir bearbeiteten Schreibaufgabe erstellt. Dabei wurden die Wissensstrukturen, die in deinem Text verfasst sind, mittels der Software T-MITOCAR computer-linguistisch extrahiert, grafisch dargestellt, mit einem Mustertext verglichen und mittels der Software TASA für dich interpretiert. Das Ergebnis dieser Analyse steht dir hier zur Verfügung. Wir vom tech4comp-Team wü̈nschen dir viel Spaß und Erfolg mit dieser Analyse deiner im Text verfassten Vorstellungen.

In diesem Abschnitt wird das Wissensmodell aus deinem Text präsentiert und mit dem Modell des Mustertextes verglichen. Die folgende Grafik (Abbildung 1) zeigt die Repräsentation deiner Ausarbeitung zum Thema -subject-:

| -graph2- |
|:--:|
| *Abbildung (1): Dein Modell zum Thema -subject-* |

Mittels einer computer-linguistischen Analysesoftware wurde die oben stehende Grafik auf der Grundlage deines Textes erstellt. Sie enthält die wichtigsten in deinem Text vorkommenden Vorstellungen zum Thema -subject-. In deinem Text besonders stark assoziierte Begriffsverbindungen werden in roter Farbe dargestellt. Du erkennst dies auch daran, dass die Assoziationsstärke zwischen den Begriffen besonders hoch ist (das ist die kleine Zahl außerhalb der Klammer, und sie liegt zwischen 0 und 1). Immer noch wichtige aber weniger stark assoziierte Begriffsverbindungen werden in Blau dargestellt.
Achte bei der Einschätzung deines Modells auch auf das gesamte Netzwerk: Welche Begriffe sind ganz zentral und gehen viele Verbindungen mit benachbarten Konzepten ein und welche sind eher am Rande oder vielleicht sogar kaum mit dem Modell verbunden?

Die folgende Grafik (Abbildung 2) zeigt die Repräsentation des Mustertextes zum Thema -subject-, das sogenannte Expertenmodell:

| -graph1- |
|:--:|
| *Abbildung (2): Das Expertenmodell zum Thema -subject-* |

Mittels einer computerlinguistischen Analysesoftware (T-MITOCAR) wurde die oben stehende Grafik auf der Grundlage des Mustertextes erstellt. Sie enthält die wichtigsten in diesem Text vorkommenden Vorstellungen zum Thema -subject-.

## 2. Vergleich der Texte: Wissensmodelle

In diesem Abschnitt wird dein Text zum Thema -subject- mit dem Mustertext verglichen. Dieser Vergleich berücksichtigt nicht nur die vorhandenen Begriffe, sondern analysiert das sogenannte ‘Wissensmodell’, die Verbindung zwischen den Begriffen. Bei Unterschieden in den Modellen geht es weniger um die vorhandenen Begriffe, sondern um die Art, wie diese Begriffe untereinander vernetzt sind. Dies kann im einfacheren Fall das Vorhandensein anderer Verknüpfungen betreffen, kann jedoch ebenso auf grundlegende Unterschiede im Wissensmodell hinweisen.

Schau dir in Ruhe die abweichenden Strukturen an. Kannst du etwas daraus lernen, wie die Begriffe unterschiedlich verknüpft sind? Welche Aussagen korrespondieren deiner Meinung nach mit den jeweiligen Strukturen? Ist im Expertenmodell etwas zu finden, was bei dir noch gefehlt hat? Kannst du die Unterschiede zwischen den Modellen auch im Text nachvollziehen?

Zwischen deinem Text und dem Expertentext zum Thema -subject- ergeben sich somit einige Unterschiede in den Modellstrukturen. Diese Gemeinsamkeiten und Unterschiede werden im Folgenden zusammengefasst.

### 2.1 Gemeinsame Verbindungen

Die folgende Abbildung zeigt, welche Verbindungen sowohl in deinem Text als auch im Expertentext vorkommen. In dieser Hinsicht stimmt dein Text also nicht nur begrifflich, sondern auch im Hinblick auf die Verknüpfung dieser Begriffe mit dem Expertenmodell überein.

| -graphDiffA- |
|:--:|
| *Abbildung 3: Gemeinsame Verbindungen zum Thema -subject-* |

### 2.2 Hinzugekommene Verbindungen

Die folgende Abbildung zeigt, welche Verbindungen in deinem Text vorhanden sind, jedoch nicht im Expertentext:

| -graphDiffB- |
|:--:|
| *Abbildung 4: Hinzugekommene Verbindungen zum Thema -subject-* |

Entspricht es deiner Absicht beim Schreiben, dass diese Aspekte in deinem Text ausdrücklich fokussiert werden?

### 2.3 Nicht aufgenommene Verbindungen

Folgende Verbindungen sind im Expertentext vorhanden, finden sich jedoch nicht in deinem Text wieder:

| -graphIntersection- |
|:--:|
| *Abbildung 5: Nicht aufgenommene Verbindungen* |

Dass diese Verbindungen in deinem Text nicht vorkommen, kann mehrere Gründen haben. Entweder du hast diese Wissenselemente in deiner Darstellung bewusst ausgeklammert oder du hast zwar einzelne Begriffe hieraus verwendet, diese jedoch in deinem Text nicht ausdrücklich miteinander in Verbindung gebracht. Überlege, ob du die dargestellten Verbindungen für deine Ausführungen nicht benötigst. Kannst du dennoch den Zusammenhang dieser Verbindungen zum Thema -subject- nachvollziehen?

## 3. Abschließende Bemerkung
Bei dem gesamten Analysevorgang zur Erstellung dieses Dokuments wurden keine personenbezogenen Daten von dir gespeichert. Auch deine Texte werden von den Systemen an der Martin-Luther-Universität Halle-Wittenberg wieder vollständig gelöscht.
Diese Auswertung ist Teil des vom BmBF geförderten Verbundprojektes tech4comp. Die Software für diese Auswertung wurde von Prof. Dr. Pablo Pirnay-Dummer für das Projekt entwickelt und steht Projektpartnern und kooperierenden Projekten zur Analyse zur Verfügung. Die Software hatte zum Zeitpunkt dieser Auswertung die Version 0.0.3.
Das Entwicklungsteam der Software hat möglicherweise keinen Einfluss darauf, in welchem didaktischen Rahmen dieser Software eingesetzt wird. Zur Anwendung der Software in deiner Lehrveranstaltung frage bitte deine Dozentinnen und Dozenten.
